var Room = function () {

  this.defineProperties({
    subject: {type: 'string'},
    tags: {type: 'array'}
  });
  
  this.hasMany('Users'); //Participants
  this.hasMany('Messages'); //Messages in Room
  
};

Room = geddy.model.register('Room', Room);

