var Message = function () {

  this.defineProperties({
    text: {type: 'string'}
  , user: {type: 'object', required: true}
  });
  
  this.hasOne('User'); //Creator of Message
};

Message = geddy.model.register('Message', Message);

