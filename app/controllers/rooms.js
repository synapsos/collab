var Rooms = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

  this.index = function (req, resp, params) {
    var self = this;

    geddy.model.Room.all(function(err, rooms) {
      self.respond({params: params, rooms: rooms});
    });
  };

  this.add = function (req, resp, params) {
    this.respond({params: params});
  };

  this.create = function (req, resp, params) {
    params.id = params.id || geddy.string.uuid(10);

    var self = this
      , room = geddy.model.Room.create(params);

    if (!room.isValid()) {
      params.errors = room.errors;
      self.transfer('add');
    }

    room.save(function(err, data) {
      if (err) {
        params.errors = err;
        self.transfer('add');
      } else {
        self.redirect({controller: self.name});
      }
    });
  };

  this.show = function (req, resp, params) {
    var self = this;

    geddy.model.Room.first(params.id, function(err, room) {
      if (!room) {
        var err = new Error();
        err.statusCode = 400;
        self.error(err);
      } else {
        self.respond({params: params, room: room.toObj()});
      }
    });
  };

  this.edit = function (req, resp, params) {
    var self = this;

    geddy.model.Room.first(params.id, function(err, room) {
      if (!room) {
        var err = new Error();
        err.statusCode = 400;
        self.error(err);
      } else {
        self.respond({params: params, room: room});
      }
    });
  };

  this.update = function (req, resp, params) {
    var self = this;

    geddy.model.Room.first(params.id, function(err, room) {
      room.updateProperties(params);
      if (!room.isValid()) {
        params.errors = room.errors;
        self.transfer('edit');
      }

      room.save(function(err, data) {
        if (err) {
          params.errors = err;
          self.transfer('edit');
        } else {
          self.redirect({controller: self.name});
        }
      });
    });
  };

  this.destroy = function (req, resp, params) {
    var self = this;

    geddy.model.Room.remove(params.id, function(err) {
      if (err) {
        params.errors = err;
        self.transfer('edit');
      } else {
        self.redirect({controller: self.name});
      }
    });
  };

};

exports.Rooms = Rooms;
